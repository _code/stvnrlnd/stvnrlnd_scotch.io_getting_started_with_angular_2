import { Component, Output, EventEmitter } from '@angular/core';
import { User } from '../shared/models/user'

@Component({
    selector: 'user-form',
    template:
    `
    <form #form="ngForm" (ngSubmit)="onSubmit()" *ngIf="active">
        <div class="form-group" [ngClass]="{'has-error': name.invalid && name.touched}">
            <input [(ngModel)]="newUser.name" #name="ngModel" type="text" class="form-control" name="name" placeholder="Name" required>
            <span class="help-block" *ngIf="name.invalid && name.touched">Name is required</span>
        </div>
        <div class="form-group" [ngClass]="{'has-error': username.invalid && username.touched}">
            <input [(ngModel)]="newUser.username" #username="ngModel" type="text" class="form-control" name="username" placeholder="Username" required>
            <span class="help-block" *ngIf="username.invalid && username.touched">Username is required</span>
        </div>
        <div class="form-group">
            <button [disabled]="form.invalid" type="submit" class="btn btn-block btn-primary">Create User</button>
        </div>
    </form>
    `,
    styles: [
        `
        form {
            padding: 10px;
            background: #ECF0F1;
            border-radius: 3px;
        }
        `
    ]
})

export class UserFormComponent {
    @Output() userCreated = new EventEmitter();
    newUser: User = new User();
    active: boolean = true;
    onSubmit() {
        this.userCreated.emit({ user: this.newUser })
        this.newUser = new User();
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }
}