import { Component, Input } from '@angular/core';

import { User } from '../shared/models/user';

@Component({
    selector: 'user-profile',
    template: 
    `
    <div *ngIf="user">
        <div class="page-header">
            <h1>{{ user.name }}</h1>
            <p><input class="form-control" [(ngModel)]="user.name"></p>
        </div>
        <p><strong>User ID</strong>: {{user.id}}<p>
        <p><strong>Username</strong>: {{user.username}}<p>
    </div>
    <div *ngIf="!user">
        <h1>No user has been selected yet...</h1>
    </div>
    `
})

export class UserProfileComponent {
    @Input() user: User;
}