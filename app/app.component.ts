import { Component } from '@angular/core';

import { User } from './shared/models/user';

@Component({
    selector: 'my-app',
    templateUrl:'./app/app.component.html',
    styleUrls: ['./app/app.component.css']
})

export class AppComponent {
    brand: string = 'Angular 2';
    message: string = 'Welcome';
    users: User[] = [
        { id: 1, name: 'Fox', username: 'stvnrlnd' },
        { id: 2, name: 'John', username: 'jhnsclr' },
        { id: 3, name: 'Paige', username: 'prettybird' }
    ];
    activeUser: User;
    selectUser(user) {
        this.activeUser = user;
    }
    onUserCreated(event) {
        this.users.push(event.user);
    }
}